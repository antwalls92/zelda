﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkController : MonoBehaviour {

	public float 		movementSpeed	{ get; set;}
	public Vector2 		direction		{ get; set;}
	public bool			moveLeft		{ get; set;}
	public bool			moveRight		{ get; set;}
	public bool			moveUp			{ get; set;}
	public bool			moveDown		{ get; set;}
	public bool			sword			{ get; set;}
	public bool			grabbingSword	{ get; set;}
	public bool			releasingSword	{ get; set;}
	public float 		forwardSpeed	{ get; set;}
	public float 		sideSpeed		{ get; set;}
	public Vector2 		input			{ get; set;}
	public Sprite sprite_up;
	public Sprite sprite_down;
	public Sprite sprite_right;
	public Sprite sprite_left;


	// Use this for initialization
	void Start () {
		movementSpeed = 1.5f;
		direction = new Vector2 (-1, 0); 
		moveDown = true;
	}
	void ChangeMovementState(Vector2 direction_vector)
	{
		moveLeft = Vector2.Angle (direction_vector, Vector2.down) == 0;
		moveRight = Vector2.Angle (direction_vector, Vector2.up) == 0;
		moveUp = Vector2.Angle (direction_vector, Vector2.right) == 0;
		moveDown = Vector2.Angle (direction_vector, Vector2.left) == 0;

	}

	bool DirectionChanged(Vector2 previous, Vector2 actual)
	{
		return Vector2.Angle(previous, actual) != 0 && actual != Vector2.zero;
	}

	void ChangeDirection(Animator animator)
	{
		animator.SetBool ("moveright", moveRight);
		animator.SetBool ("moveleft", moveLeft);
		animator.SetBool ("moveup", moveUp);
		animator.SetBool ("movedown", moveDown);
	}
	void DragSword (){
		/**var sword = transform.Find ("sword").gameObject;
		var sword_renderer = sword.GetComponent<SpriteRenderer> ();
		sword_renderer.enabled = true;*/
		}
	void ReleaseSword (){
		/**var sword = transform.Find ("sword").gameObject;
		var sword_renderer = sword.GetComponent<SpriteRenderer> ();
		sword_renderer.enabled = false;*/
	}

	void SwordStuff()
	{
		getSwordState ();

		if (grabbingSword) {
			DragSword ();
			ChangeSwordDirection ();
		} else if (releasingSword) {
			ReleaseSword ();
		}
		var animator = GetComponent<Animator> ();
		animator.SetBool ("sword", sword);
	}

	void getSwordState()
	{
		sword = (sword || Input.GetKeyDown (KeyCode.X)) && !Input.GetKeyUp (KeyCode.X);
		grabbingSword = Input.GetKeyDown (KeyCode.X);
		releasingSword = Input.GetKeyUp (KeyCode.X);
	}

	void ChangeSwordDirection ()
	{
		var sword = transform.Find ("sword").gameObject;
		var sword_trasform = sword.GetComponent<Transform> ();
		sword_trasform.rotation = new Quaternion ();
		if(moveUp){
			sword_trasform.position = transform.position + new Vector3(-0.2f, 0.9f);
			sword_trasform.Rotate(Vector3.forward * 0)  ;

		}
		if (moveDown) {
			sword_trasform.position = transform.position + new Vector3(0.2f, -0.9f); 
			sword_trasform.Rotate(Vector3.forward * 180)  ;

		}
		if (moveLeft) {
			sword_trasform.position = transform.position + new Vector3(-0.75f, -0.25f);
			sword_trasform.Rotate(Vector3.forward * 90)  ;

		}
		if (moveRight) {
			sword_trasform.position = transform.position + new Vector3(0.75f, -0.25f);
			sword_trasform.Rotate(Vector3.forward * 270)  ;
		}
	}
	void ChangeIdleSprite ()
	{
		var sprite_renderer = transform.GetComponent<SpriteRenderer> ();
		if(moveUp){
			sprite_renderer.sprite = sprite_up;
		}
		if (moveDown) {
			sprite_renderer.sprite = sprite_down;
		}
		if (moveLeft) {
			sprite_renderer.sprite = sprite_left;
		}
		if (moveRight) {
			sprite_renderer.sprite = sprite_right;
		}
	}

	void GetInput()
	{
		forwardSpeed = Input.GetAxis("Vertical") ;
		sideSpeed = Input.GetAxis("Horizontal") ;
		input = new Vector2 (forwardSpeed, sideSpeed);
	}

	void MoveStuff()
	{
		var animator = GetComponent<Animator> ();
		var directionchanged = DirectionChanged (direction, input);

		if (input.magnitude == 0) {
			ChangeIdleSprite ();
		} else {
			
			if (directionchanged && !sword) {
				ChangeMovementState (input);
				ChangeDirection (animator); 
			}
			animator.SetBool ("directionchanged", directionchanged);
			direction = input;

			animator.SetFloat("speed",input.magnitude);

			var speedx = sideSpeed > 0 ? 1 : -1;
			var speedy = forwardSpeed > 0 ? 1 : -1;

			speedx = sideSpeed == 0 ? 0 : speedx;
			speedy = forwardSpeed == 0 ? 0 : speedy;

			transform.Translate (Vector3.left * Time.deltaTime * (speedx) 		* movementSpeed *    -1);
			transform.Translate (Vector3.down * Time.deltaTime * (speedy) 	* movementSpeed * 	-1);
		}
	}

	// Update is called once per frame
	void Update () {
		GetInput ();
		SwordStuff ();
		MoveStuff ();
	}
}
